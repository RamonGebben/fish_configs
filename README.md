# Super Command line Interface

## Getting started

    brew install fish
    git clone git@bitbucket.org:RamonGebben/fish_configs.git
    cd fish_configs/
    cp -r * ~/.config/fish/.

And launch fish with
    
    fish

![iTerm](https://s3-eu-west-1.amazonaws.com/uploads-eu.hipchat.com/103878/1086243/BGv4u5Zvy8wMgIO/upload.png)


## Git

Git still works the same it has a new syntax now.

To add everything in current directory

     :add

To commit

     :commit "This is a commit message"

To push to master

     :push

And to a specific branch

     :push <branch>

